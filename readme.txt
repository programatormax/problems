===========================================
                  INTRO
===========================================

0. Docs dir contain tasks points.

1. All tasks built in C# (Visual Studio Code IDE)

2. First of all install please DOTNET from directory dotnet or 
   https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/sdk-6.0.202-windows-x64-installer

3. Start

===========================================
                 PROBLEM 1
===========================================

Location: .\Problem1\bin\Debug\net6.0


Usage: Problem1 [-p processfullpath] [-i interval] [-j journalfullpath]
Example: Problem1 -p C:\HH\notepad.exe -i 10 -j C:\LOG\logdata.log
Example: Problem1.exe -p "c:\Program Files\kdiff3\kdiff3.exe" -i 10 -j c:\programdata\difc.json

Options: 
   -p processfullpath    Pullpath process location
   -i interval           Executing interval (in seconds)
   -j journalfullpath    Log fullpath directory


logdata format is json array objects:
[
  {
    "procname": "kdiff3",
    "datetime": 1651350701,
    "workingset": 30834688,
    "privatememory": 14749696,
    "handlescount": 277,
    "status": 2,
    "cpusage": 1
  }
]

procname      - name of process
datetime      - Datetime snapshot process (unixtime format)
workingset    - Physical memory usage (WorkingSet)
privatememory - Private memory Size (Private Bytes)
handlescount  - Handles count
status        - Status of process (2 - running, 1 - not respond, 0 - not running)
cpuusage      - usage cpu  %

My Feedback:
Task says [Format of stored data should support automated parsing to potentially allow, for example, drawing of charts].
I would prefer not to create a file and use the PROMETHEUS protocol support to draw diagrams in GRAFANA.

===========================================
                 PROBLEM 2
===========================================

Location: .\Problem2\bin\Debug\net6.0


Usage: Problem2 [-s sourcedir] [-d destdir] [-i interval [-j journalfullpath]
Example: Problem2 -s C:\HH -d D:\HH -i 3600 -j C:\LOG\logdata.log

Options: 
   -s sourcedir          Source directory to sync
   -d destdir            Dest directory to sync
   -i interval           Synchronization interval (in seconds)
   -j journalfullpath    Log fullpath directory

===========================================
                 PROBLEM 3
===========================================

Location: .\Problem3\Server\bin\Debug\net6.0

About server:
 
       ports:      8000,8001
       protocol:   HTTP

NOTE:

  The default server directory C:\ProgramData\ServerDataLogger is uses as database.
  You may need to disable the filewall and give the server additional rights to get access ports 8000 and 8001. 

About client protocol:
  Location: .\Problem3\Client\bin\Debug\net6.0

  client to server request http://localhost:8000/token?uid=demo
  server to client response body contain ucode (ZGVtb2RlbW9kZW1v)
  client to server request http://localhost:8000/send?uid=demo&ucode=ZGVtb2RlbW9kZW1v&utext=Hello
  server to client response http: code 200 is ok(log this event to logfile), else 404
 
   
