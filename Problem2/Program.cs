﻿using System;
using System.IO;
using System.Threading;


class SyncDir
{

    static class Logvar
    {
        public static string JornalDirectory;
    }

    public static void Logger(string log)
    {
      Console.WriteLine(log);
      log += "\r\n";
      File.AppendAllText(Logvar.JornalDirectory,log);
    }

    public static void Sync(string sourceDirectory, string targetDirectory, string loggerDirectory)
    {
        DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
        DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);
        SyncAll(diSource, diTarget);
    }

    public static void SyncAll(DirectoryInfo source, DirectoryInfo target) 
    {
        Directory.CreateDirectory(target.FullName);

        foreach (FileInfo fi in source.GetFiles())
        {
            string synFile = target.FullName +"\\" + fi.Name;
            string delFile = source.FullName +"\\" + fi.Name;
            if (File.Exists(synFile)) 
            {
              Logger(DateTime.Now.ToString("MMM dd yyyy,hh:mm:ss") +" Skip " + delFile);
            }
            else
            {
              File.Delete(delFile);
              Logger(DateTime.Now.ToString("MMM dd yyyy,hh:mm:ss") +" Delete " + delFile);
            } 
        }

        foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
        {
            Logger(DateTime.Now.ToString("MMM dd yyyy,hh:mm:ss") +" Skip " + source.FullName);
            DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
            SyncAll(diSourceSubDir, nextTargetSubDir);
        }
    }

    public static void Copy(string sourceDirectory, string targetDirectory, string loggerDirectory)
    {
        DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
        DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);
        CopyAll(diSource, diTarget);
    }

    public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
    {
        Directory.CreateDirectory(target.FullName);

        foreach (FileInfo fi in source.GetFiles())
        {
            string synFile = target.FullName +"\\" + fi.Name;
            string delFile = source.FullName +"\\" + fi.Name;

            if (File.Exists(synFile)) 
            {
              long length1 = new System.IO.FileInfo(synFile).Length;
              long length2 = new System.IO.FileInfo(delFile).Length;
              if (length1 == length2)
              {
                Logger(DateTime.Now.ToString("MMM dd yyyy,hh:mm:ss") +" Skip " + delFile + " "+ synFile);   
              } 
              else
              {
                Logger(DateTime.Now.ToString("MMM dd yyyy,hh:mm:ss") +" Copy " + delFile + " "+ synFile);
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);    
              }
            }
            else
            {
              Logger(DateTime.Now.ToString("MMM dd yyyy,hh:mm:ss") +" Create " + delFile + " "+ synFile);
              fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }    
        }

        foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
        {
            if (Directory.Exists(target.FullName))
            {
              Logger(DateTime.Now.ToString("MMM dd yyyy,hh:mm:ss") + " Skip "+ source.FullName + " " + target.FullName);
            }
            else
            {
              Logger(DateTime.Now.ToString("MMM dd yyyy,hh:mm:ss") + " Create "+ source.FullName + " " + target.FullName);
            }
            DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
            CopyAll(diSourceSubDir, nextTargetSubDir);
        }
    }

    public static void Main(string[] args)
    {
      if (args.Length==0)
      {
        Console.WriteLine("");
        Console.WriteLine("================");  
        Console.WriteLine("Sync folder v1.0");
        Console.WriteLine("================");
        Console.WriteLine("");  
        Console.WriteLine("Usage: Problem2 [-s sourcedir] [-d destdir] [-i interval [-j journalfullpath]");
        Console.WriteLine("Example: Problem2 -s C:\\HH -d D:\\HH -i 10 -j C:\\LOG\\logdata.log");
        Console.WriteLine("");
        Console.WriteLine("Options: ");
        Console.WriteLine("   -s sourcedir          Source directory to sync");
        Console.WriteLine("   -d destdir            Dest directory to sync");
        Console.WriteLine("   -i interval           Synchronization interval (in seconds)");
        Console.WriteLine("   -j journalfullpath    Log fullpath directory");
        Console.WriteLine("");
      }
      else
      {
       if (args.Length==8)
       {
          string sourceDirectory = @"";
          string targetDirectory = @"";
          string JornalDirectory = @"";
          int Intr = 0;
          long Count = 0;
          for (int i = 0; i < args.Length; i++)
          {
            if (args[i] == "-s") { sourceDirectory = args[i + 1];}
            if (args[i] == "-d") { targetDirectory = args[i + 1];}
            if (args[i] == "-j") { JornalDirectory = args[i + 1];}
            if (args[i] == "-i") { Intr = Int32.Parse(args[i + 1]);}
          }
         
          Logvar.JornalDirectory = JornalDirectory;

          if (Directory.Exists(sourceDirectory))
          {

            while (Count == 0)
            {
              Sync(targetDirectory,sourceDirectory,JornalDirectory);
              Copy(sourceDirectory,targetDirectory,JornalDirectory); 
              Console.WriteLine("Interval: {0}",Intr);
              Thread.Sleep(Intr * 1000);
              Thread.Sleep(100);
            }
          }
          else
          {
            Console.WriteLine("Error: Source directory not exists");  
          }  
       }
       else
       {
        Console.WriteLine("Error: All arguments must be filled.");   
       }
      }   
    }
}
