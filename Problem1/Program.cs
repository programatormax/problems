﻿using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Text.Json;

namespace ProcessMonitor
{


    class ProcessMonitorClass
    {

         class Snapshot
         {
           public string procname { get; set; } 
           public long datetime { get; set; }
           public long workingset { get; set; }
           public long privatememory { get; set; }
           public long handlescount { get; set; }
           public long status { get; set; }
           public long cpusage { get; set; }

           public Snapshot(string Name, long Dtm, long Prvbts, long Wzs, long Whnd, long Status, long Cpusage) 
           {
             procname = Name;
             datetime = Dtm;
             workingset = Prvbts;
             privatememory = Wzs;
             handlescount = Whnd;
             status = Status;
             cpusage = Cpusage;
           }
         }

        private static DateTime lastTime;
        private static TimeSpan lastTotalProcessorTime;
        private static DateTime curTime;
        private static TimeSpan curTotalProcessorTime;

        public static void Log(object obj, string fileName)
        {  var options = new JsonSerializerOptions() 
           { 
            WriteIndented = true
           };
            var jsonString = JsonSerializer.Serialize(obj, options);
              File.WriteAllText(fileName, jsonString);
        }


        public static void Main(string[] args)
        {
         
         

            long peakPagedMem   = 0,
                 peakWorkingSet = 0,
                 peakVirtualMem = 0,
                 currStatus     = 0;
                 List <Snapshot> Report = new List<Snapshot>(); 
        DateTime lastTime;
        TimeSpan lastTotalProcessorTime;
        DateTime curTime;
        TimeSpan curTotalProcessorTime;
                 lastTime = new DateTime();
                 lastTime = DateTime.Now;
          double CPUUsage = 0;
          string processDirectory = @"";
          string JornalDirectory  = @"";
          int    Intr = 0;
          long   Count = 0;
                 

         if (args.Length==0)
            {
               Console.WriteLine("");
               Console.WriteLine("====================");  
               Console.WriteLine("Process control v1.0");
               Console.WriteLine("====================");
               Console.WriteLine("");  
               Console.WriteLine("Usage: Problem1 [-p processfullpath] [-i interval] [-j journalfullpath]");
               Console.WriteLine("Example: Problem1 -p C:\\HH\\notepad.exe -i 10 -j C:\\LOG\\notepad.log");
               Console.WriteLine("Example: Problem1 -p \"c:\\Program Files\\kdiff3\\kdiff3.exe\" -i 10 -j c:\\programdata\\kdiff3.log");
               Console.WriteLine("");
               Console.WriteLine("Options: ");
               Console.WriteLine("   -p processfullpath    Pullpath process location");
               Console.WriteLine("   -i interval           Executing interval (in seconds)");
               Console.WriteLine("   -j journalfullpath    Log fullpath directory");
               Console.WriteLine("");
               System.Environment.Exit(0);
            }
            else
            {

              if (args.Length==6)
               {  
                 for (int i = 0; i < args.Length; i++)
                 {
                   if (args[i] == "-p") { processDirectory = args[i + 1];}
                   if (args[i] == "-j") { JornalDirectory = args[i + 1];}
                   if (args[i] == "-i") { Intr = Int32.Parse(args[i + 1]);}
                 }
               }
               else
               {
                 Console.WriteLine("Error: All arguments must be filled.");  
                 System.Environment.Exit(0);    
               }
            }
        
            
            if (File.Exists(processDirectory)) 
            {
              //Time for fun. Malevich's Black Square ;-)))
            }
            else 
            {
              Console.WriteLine("Error: Process not exists");
              System.Environment.Exit(0);
            }

            onceagain:

            using (Process myProcess = Process.Start(processDirectory))
            {
                lastTotalProcessorTime = myProcess.TotalProcessorTime;
                
                do
                {
                    if (!myProcess.HasExited)
                    {
                        
                        myProcess.Refresh();
                       
  
                        
                        curTime = DateTime.Now;
                        curTotalProcessorTime = myProcess.TotalProcessorTime;
                        CPUUsage = (curTotalProcessorTime.TotalMilliseconds - lastTotalProcessorTime.TotalMilliseconds) / curTime.Subtract(lastTime).TotalMilliseconds / Convert.ToDouble(Environment.ProcessorCount);
                        lastTime = curTime;
                        lastTotalProcessorTime = curTotalProcessorTime;

                        Console.Clear(); 
                        Console.WriteLine();
                        Console.WriteLine($"{myProcess} -");                      
                        Console.WriteLine($"  CPU Usage %               : {CPUUsage * 100}");
                        Console.WriteLine($"  Physical memory usage     : {myProcess.WorkingSet64}");
                        Console.WriteLine($"  Base priority             : {myProcess.BasePriority}");
                        Console.WriteLine($"  Priority class            : {myProcess.PriorityClass}");
                        Console.WriteLine($"  User processor time       : {myProcess.UserProcessorTime}");
                        Console.WriteLine($"  Privileged processor time : {myProcess.PrivilegedProcessorTime}");
                        Console.WriteLine($"  Total processor time      : {myProcess.TotalProcessorTime}");
                        Console.WriteLine($"  Paged system memory size  : {myProcess.PagedSystemMemorySize64}");
                        Console.WriteLine($"  Paged memory size         : {myProcess.PagedMemorySize64}");
                        Console.WriteLine($"  Private memory Size       : {myProcess.PrivateMemorySize64}");
                        Console.WriteLine($"  Open handlers count       : {myProcess.HandleCount}");

                        peakPagedMem   = myProcess.PeakPagedMemorySize64;
                        peakVirtualMem = myProcess.PeakVirtualMemorySize64;
                        peakWorkingSet = myProcess.PeakWorkingSet64;

                        if (myProcess.Responding)
                        {
                            Console.WriteLine("  Status                    : Running");
                            currStatus = 2;
                        }
                        else
                        {
                            Console.WriteLine("  Status                    : Not respond");
                            currStatus = 1;
                        }

                        Report.Add(new Snapshot(myProcess.ProcessName,DateTimeOffset.UtcNow.ToUnixTimeSeconds(),myProcess.WorkingSet64,myProcess.PrivateMemorySize64,myProcess.HandleCount,currStatus,Convert.ToInt64(CPUUsage * 100)));
                    }
                }
                while (!myProcess.WaitForExit(1000));

                Report.Add(new Snapshot(myProcess.ProcessName, DateTimeOffset.UtcNow.ToUnixTimeSeconds(),0,0,0,myProcess.ExitCode,0));

                Console.WriteLine();
                Console.WriteLine($"  Process exit code          : {myProcess.ExitCode}");
                Console.WriteLine($"  Peak physical memory usage : {peakWorkingSet}");
                Console.WriteLine($"  Peak paged memory usage    : {peakPagedMem}");
                Console.WriteLine($"  Peak virtual memory usage  : {peakVirtualMem}");
                Log(Report,JornalDirectory);
                Console.WriteLine();
                Console.WriteLine($"  Interval : {Intr}");
                Thread.Sleep(Intr * 1000);
                goto onceagain;
            }

        }
    }


}