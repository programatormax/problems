﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using static System.Web.HttpUtility;

namespace HttpListenerServer
{
    class HttpServer
    {
        public static HttpListener listener;
        public static string url1    = "http://*:8000/";
        public static string url2    = "http://*:8001/";
        public static string storage = "C:\\ProgramData\\ServerDataLogger\\";
        public static Object obj = new Object();
        public static void Logger(string log, bool flag)
        {
         bool locked = false;
         var tempObj = obj;
         try
         {
            Monitor.Enter(tempObj, ref locked);
            log = "| " + DateTime.Now.ToString("MMM dd yyyy,hh:mm:ss") + " | " + log;
            if (flag == true) 
            {
              Console.WriteLine(log);
            }
            else
            {
              Console.WriteLine(log);
              log += "\r\n";
              File.AppendAllText(storage+"ServerDataLogger.log", log);
            }
         } 
         finally
         {
           if (locked)
           {
            Monitor.Exit(tempObj);
           }
         }
        }
      
        public static string Base64Encode(string plainText) 
        {
          var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
          return System.Convert.ToBase64String(plainTextBytes);
        }

        public static async Task HandleIncomingConnections()
        {
            int ResponseCode = 200;
            string ResponseData = "Nothing";
            bool runServer = true;
            Random rnd = new Random();
           

            while (runServer)
            {
                ResponseCode = 200;
                ResponseData = "Nothing";
                HttpListenerContext ctx = await listener.GetContextAsync();
                HttpListenerRequest req = ctx.Request;
                HttpListenerResponse resp = ctx.Response;
                string remoteip = ctx.Request.RemoteEndPoint.Address.ToString();
                if (remoteip=="::1") { remoteip = "localhost";}
                string remoteport = ctx.Request.RemoteEndPoint.Port.ToString();
                string localip = ctx.Request.LocalEndPoint.Address.ToString();
                if (localip=="::1") { localip = "localhost";} 
                string localport = ctx.Request.LocalEndPoint.Port.ToString();
                string sessionid = remoteip + ":" + remoteport + ":"+ localip + ":" + localport;
               
              

                Logger(sessionid + " | Request: "+req.HttpMethod+" "+req.Url.ToString(), true);  
 
                if ((req.HttpMethod == "GET") && (req.Url.AbsolutePath == "/token"))
                {
                  Uri myUri = new Uri(req.Url.ToString());
                  string uid = HttpUtility.ParseQueryString(myUri.Query).Get("uid");
                  int val = rnd.Next(10000, 65535);
                  string ucode = Base64Encode(uid + val.ToString());
                  File.WriteAllText(storage + uid + ".ucode", ucode);  
                  Logger(sessionid + " | Autorization: uid=" + uid + " ucode=" + ucode, true);
                  ResponseCode = 200;
                  ResponseData = ucode;
                }

                if ((req.HttpMethod == "GET") && (req.Url.AbsolutePath == "/send"))
                {
                  Uri myUri = new Uri(req.Url.ToString());
                  string uid = HttpUtility.ParseQueryString(myUri.Query).Get("uid");
                  string ucode = HttpUtility.ParseQueryString(myUri.Query).Get("ucode");
                  string utext = HttpUtility.ParseQueryString(myUri.Query).Get("utext");
                  string line = "";
                  if (File.Exists(storage + uid + ".ucode"))
                  {
                    StreamReader sr = new StreamReader(storage + uid + ".ucode");
                    line = sr.ReadLine();
                    if (ucode == line) 
                    {
                      Logger(sessionid+ " | Received: uid=" + uid + " ucode=" + ucode +" utext="+ utext, false);  
                      ResponseCode = 200;
                      ResponseData = "Text sent";
                    }
                    else
                    {
                      Logger(sessionid + " | Decline: uid=" + uid + " ucode=" + ucode, true);  
                      ResponseCode = 404;
                      ResponseData = "Not found";   
                    }
                    sr.Close();
                  }
                  else
                  {
                    Logger(sessionid + " | Decline: uid=" + uid + " ucode=" + ucode, true);  
                    ResponseCode = 404;
                    ResponseData = "Not found";
                  }
                }

                Logger(sessionid + " | Response: "+ResponseCode.ToString()+" "+ResponseData, true);

                byte[] data = Encoding.UTF8.GetBytes(ResponseData);
                resp.StatusCode = ResponseCode; 
                resp.ContentType = "text/plain";
                resp.ContentEncoding = Encoding.UTF8;
                resp.ContentLength64 = data.LongLength;

                await resp.OutputStream.WriteAsync(data, 0, data.Length);
                resp.Close();
            }
        }

        public static void Main(string[] args)
        {
            Directory.CreateDirectory(storage);
            listener = new HttpListener();
            listener.Prefixes.Add(url1);
            listener.Prefixes.Add(url2);
            listener.Start();
            Console.WriteLine("Listening on {0}", url1);
            Console.WriteLine("Listening on {0}", url2);
            Task listenTask = HandleIncomingConnections();
            listenTask.GetAwaiter().GetResult();
            listener.Close();
        }
    }
}